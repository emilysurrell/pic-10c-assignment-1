# README #

## This repository is for PIC 10C, Assignment 1 at UCLA ##

Quarter: Spring 2020

Instructor: Ricardo Salazar

Student: Emily Surrell

### The Assignment ###

* This project is intended to create a templated Pic10b::vector class (Option A)
* Student work is contained in [pic10b_vector.h](https://bitbucket.org/emilysurrell/pic-10c-assignment-1/src/master/pic10b_vector.h)
* Version Control: See my [Commits](https://bitbucket.org/emilysurrell/pic-10c-assignment-1/commits/) for this repository and notice my [.gitignore file](https://bitbucket.org/emilysurrell/pic-10c-assignment-1/src/master/.gitignore)
* Read the [Assignment Description](https://www.pic.ucla.edu/~rsalazar/pic10b/assignments/hw4/)

### Running the Code in Visual Studio 2019###

#### Step 1: Clone the Repository
1. Open Visual Studio 2019
1. Select "Continue without Code"
1. Under "View" select "Team Explorer"
1. Under "Local Git Repositories" select "Clone"
1. Copy and paste this URL:  https://emilysurrell@bitbucket.org/emilysurrell/pic-10c-assignment-1.git
1. Choose an empty folder on your computer to save this repository
#### Step 2: Create a Visual Studio Project
1. Under "File" select "New Project" and create an "Empty Project"
1. Under "View" select "Solution Explorer"
1. Right-click "Header Files" and under "Add" select "Existing Item"
1. Find where you saved this repository, and select [pic10b_vector.h](https://bitbucket.org/emilysurrell/pic-10c-assignment-1/src/master/pic10b_vector.h)
1. Right-click "Source Files" and under "Add" select "Existing Item"
1. Find where you saved this repository, and select one of the three drivers:
	* [01-driver_int.cpp](https://bitbucket.org/emilysurrell/pic-10c-assignment-1/src/master/01-driver_int.cpp)
	* [02-driver_double.cpp](https://bitbucket.org/emilysurrell/pic-10c-assignment-1/src/master/02-driver_double.cpp)
	* [03-driver_string.cpp](https://bitbucket.org/emilysurrell/pic-10c-assignment-1/src/master/03-driver_string.cpp)
#### Step 3: Compile and Run the Code
1. Compile: Under "Build" select "Build *ProjectName*"
1. Run: Under "Debug" select "Start Without Debugging"

### What You Expect to See ###
* Running [pic10b_vector.h](https://bitbucket.org/emilysurrell/pic-10c-assignment-1/src/master/pic10b_vector.h) with [01-driver_int.cpp](https://bitbucket.org/emilysurrell/pic-10c-assignment-1/src/master/01-driver_int.cpp) should produce a console output that looks like [output_int.txt](https://bitbucket.org/emilysurrell/pic-10c-assignment-1/src/master/output_int.txt)
* Running [pic10b_vector.h](https://bitbucket.org/emilysurrell/pic-10c-assignment-1/src/master/pic10b_vector.h) with [02-driver_double.cpp](https://bitbucket.org/emilysurrell/pic-10c-assignment-1/src/master/02-driver_double.cpp) should produce a console output that looks like [output_double.txt](https://bitbucket.org/emilysurrell/pic-10c-assignment-1/src/master/output_double.txt)
* Running [pic10b_vector.h](https://bitbucket.org/emilysurrell/pic-10c-assignment-1/src/master/pic10b_vector.h) with [03-driver_string.cpp](https://bitbucket.org/emilysurrell/pic-10c-assignment-1/src/master/03-driver_string.cpp) should produce a console output that looks like [output_string.txt](https://bitbucket.org/emilysurrell/pic-10c-assignment-1/src/master/output_string.txt)
