#ifndef PIC10B_VECTOR_H
#define PIC10B_VECTOR_H
#include <iostream>

namespace Pic10b
{
	// class definition
	template <class T> class vector
	{
	private:
		T* data;
		int the_size; //number of elements in the vector

	public:
		vector(); //default constructor
		vector(const vector&); //copy constructor
		vector<T>& operator=(const vector&); //assignment operator
		~vector(); //destructor

		void push_back(T new_element);
		int size() const;
		double norm() const;

		T& operator[](int index);
		T operator[](int index) const;

		vector<T>& operator+=(const vector<T>& v);
	};

	// method implementations
	template <class T> vector<T>::vector()
	{
		the_size = 0;
		data = new T[the_size];
		std::cout << "xxxxxxxxxx Default constructor called" << std::endl;
	}

	template <class T> vector<T>::vector(const vector& source): data(nullptr), the_size(source.the_size)
	{
		data = new T[the_size];
		for (int i = 0; i < the_size; i++)
		{
			data[i] = source[i];
		}
		std::cout << "xxxxxxxxxx Copy constructor called" << std::endl;
	}

	template <class T> vector<T>& vector<T>::operator=(const vector& source)
	{
		if (this != &source)
		{
			the_size = source.size();
			data = new T[the_size];
			for (int i = 0; i < the_size; i++)
			{
				data[i] = source[i];
			}
		}
		std::cout << "xxxxxxxxxx Assignment operator called" << std::endl;
		return *this;
	}

	template <class T> vector<T>::~vector()
	{
		delete[] data;
		std::cout << "xxxxxxxxxx Destructor called" << std::endl;
	}

	template <class T> void vector<T>::push_back(T new_element)
	{
		data[the_size] = new_element;
		the_size++;
	}

	template <class T> int vector<T>::size() const
	{
		return the_size;
	}

	template <class T> double vector<T>::norm() const
	{
		return sqrt((*this) * (*this));
	}

	template <class T> T& vector<T>::operator[](int index)
	{
		return data[index];
	}

	template <class T> T vector<T>::operator[](int index) const
	{
		return data[index];
	}

	template <class T> vector<T>& vector<T>::operator+=(const vector<T>& v)
	{
		for (int i = 0; i < the_size; i++)
		{
			data[i] += v[i];
		}
		return *this;
	}
}

/* -------------------------OTHER FUNCTIONS-------------------------------*/

template <class T> std::ostream& operator<<(std::ostream& out, const Pic10b::vector<T>& v)
{
	out << "{" << v[0];
	for (int i = 1; i < v.size(); i++)
	{
		out << ", " << v[i];
	}
	out << "}";

	return out;
}

template <class T> Pic10b::vector<T> operator+(const Pic10b::vector<T> & v1, const Pic10b::vector<T> & v2)
{
	Pic10b::vector<T> result = v1;
	for (int i = 0; i < v1.size(); i++)
	{
		result[i] = v1[i] + v2[i];
	}
	return result;
}

Pic10b::vector<double> operator*(const Pic10b::vector<double>& v, const double& num)
{
	Pic10b::vector<double> result = v;
	for (int i = 0; i < v.size(); i++)
	{
		result[i] = v[i] * num;
	}
	return result;
}

Pic10b::vector<double> operator*(const double& num, const Pic10b::vector<double>& v)
{
	Pic10b::vector<double> result = v;
	for (int i = 0; i < v.size(); i++)
	{
		result[i] = v[i] * num;
	}
	return result;
}

Pic10b::vector<int> operator*(const Pic10b::vector<int>& v, const int& num)
{
	Pic10b::vector<int> result = v;
	for (int i = 0; i < v.size(); i++)
	{
		result[i] = v[i] * num;
	}
	return result;
}

Pic10b::vector<int> operator*(const int& num, const Pic10b::vector<int>& v)
{
	Pic10b::vector<int> result = v;
	for (int i = 0; i < v.size(); i++)
	{
		result[i] = v[i] * num;
	}
	return result;
}

Pic10b::vector<std::string> operator*(const Pic10b::vector<std::string>& v, const std::string& word)
{
	Pic10b::vector<std::string> result = v;
	for (int i = 0; i < v.size(); i++)
	{
		result[i] = v[i] + word;
	}
	return result;
}

Pic10b::vector<std::string> operator*(const std::string& word, const Pic10b::vector<std::string>& v)
{
	Pic10b::vector<std::string> result = v;
	for (int i = 0; i < v.size(); i++)
	{
		result[i] = word + v[i];
	}
	return result;
}

template <class T, class S> double operator*(const Pic10b::vector<T>& v1, const Pic10b::vector<S>& v2) //dot product
{
	double result = 0;
	for (int i = 0; i < v1.size(); i++)
	{
		result += v1[i] * v2[i];
	}
	return result;
}

template <class T, class S> bool operator<(const Pic10b::vector<T>& v1, const Pic10b::vector<S>& v2)
{
	return (v1.norm() < v2.norm());
}

template <class T, class S> bool operator<=(const Pic10b::vector<T>& v1, const Pic10b::vector<S>& v2)
{
	return (v1.norm() <= v2.norm());
}

template <class T, class S> bool operator>(const Pic10b::vector<T>& v1, const Pic10b::vector<S>& v2)
{
	return (v1.norm() > v2.norm());
}

template <class T, class S> bool operator>=(const Pic10b::vector<T>& v1, const Pic10b::vector<S>& v2)
{
	return (v1.norm() >= v2.norm());
}

template <class T, class S> bool operator==(const Pic10b::vector<T> & v1, const Pic10b::vector<S> & v2)
{
	if (v1.size() != v2.size())
		return false;
	for (int i = 0; i < v1.size(); i++)
	{
		if (v1[i] != v2[i])
			return false;
	}
	return true;
}

template <class T, class S> bool operator!=(const Pic10b::vector<T> & v1, const Pic10b::vector<S> & v2)
{
	if (v1.size() != v2.size())
		return true;
	for (int i = 0; i < v1.size(); i++)
	{
		if (v1[i] != v2[i])
			return true;
	}
	return false;
}

#endif